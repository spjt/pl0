/*
 * 

 *
 * 
 * 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "icg.h"
#include "parser.h"
#include "scanner.h"
#include "vm.h"

/* Output:
   1a: Print out tokens (e.g. 29 2 x)
   1b: Print out token names (intsym identsym x)
   2: Print "No errors, program is syntactically correct" or error
   3: Print out generated code
   4: Run program on VM
*/

FILE *vminfile;
FILE *vmoutfile;
FILE *scannerinfile;
extern char *symbolnames[]; // defined in scanner.c

// "unit testing"
void testScanner(char *filename);
void testSymTable(void);

void printLexemesNum(lexemeListNode *head);
void printLexemesText(lexemeListNode *head);

main(int argc, char *argv[]) {
    lexemeListNode *lexemeList;
    symtable *symbols;
    pcode *codelist;
    char buf[1024];
    int i;
    int args = 0;
    /* // Test for icg
        codelist = icg_test();
        icg_display();
        exit(0);
    // */

    for(i=1;i<argc;i++) {
        // printf("argv[%d] = %s\n",i,argv[i]);
        if(argv[i][0]=='-') {
            switch(argv[i][1]) {
                case 'a': args |= 1; break;
                case 'l': args |= 2; break;
                case 'v': args |= 4; break;
                default: printf("Invalid argument %s\n",argv[i]);
            }
        } else {
            if((scannerinfile = fopen(argv[i],"r")) == NULL) {
                printf("Could not open file [%s]\n\n",argv[i]);
                exit(EXIT_FAILURE);
            }
        }
    }

    if(argc < 2 || scannerinfile == NULL) {
        printf("\n%s [arguments] <inputfile>\n\n",argv[0]);
        printf("Arguments:\n\t-l: Print the list of lexemes/tokens (scanner output) to the screen\n");
        printf("\t-a: Print the generated assembly code (parser/codegen output) to the screen\n");
        printf("\t-v: Print virtual machine execution trace (virtual machine output) to the screen\n\n");
        exit(EXIT_FAILURE);
    }

    //printf("args = %d\n", args);

    /*
    printf("\nInput file:\n");
    while(!feof(scannerinfile)) {
        fgets(buf, 1024, scannerinfile);
        if(!feof(scannerinfile)) {
            printf("    %s", buf);
        }
    }
    printf("\n");
    rewind(scannerinfile);
    */
    // Run the scanner and display the output.
    lexemeList = getLexemeList(scannerinfile);
    if(args & 2) {
        printf("\nLexeme table (numerical):\n    ");
        printLexemesNum(lexemeList);
        printf("\nLexeme table (text):\n    ");
        printLexemesText(lexemeList);
        printf("\n");
    }
    // Run the parser.
    symbols = pars_init(lexemeList);
    if(args & 1) {
        printf("Parser: No errors, program is syntactically correct.\n\n");
        stab_display();
        printf("\n");
    }
    // Run the intermediate code generator.
    codelist = icg_init(lexemeList, symbols);
    if(args & 1) {
        icg_display();
        printf("\n");
    }
    // Run the code in the VM.
    if(args & 4) {
        printf("VM Output:\n");
        vmoutfile = stdout;
    } else {
        vmoutfile = NULL;
    }
    if((args & 4) || (args == 0)) // do not run for flags 1,2
        vm_runic(codelist);
    // d("return from vm_runic");
    //clean up
    icg_free(codelist);
    stab_free(symbols);
    freeLexemeList(lexemeList);
    fclose(scannerinfile);
    //printf("\nSuccess!\n");
}

void printLexemesNum(lexemeListNode *head) {
    lexemeListNode *cur;
    cur = head;
    while(cur != NULL) {
        printf("%d ", cur->type);
        if(cur->type == numbersym || cur->type == identsym) {
            printf("%s ", cur->id);
        }
        cur = cur->next;
    }
    printf("\n");
}

void printLexemesText(lexemeListNode *head) {
    lexemeListNode *cur;
    cur = head;
    while(cur != NULL) {
        printf("%ssym ", symbolnames[cur->type]);
        if(cur->type == numbersym || cur->type == identsym) {
            printf("%s ", cur->id);
        }
        cur = cur->next;
    }
    printf("\n");
}

void testScanner(char *filename) {
    lexemeListNode *head;
    scannerinfile = openFile(filename, "r");
    head = getLexemeList(scannerinfile);
    while(head->next != NULL) {
        printf("id: %s, type: %d\n", head->id, head->type);
        head = head->next;
    }
    while(head->prev != NULL) {
        printf("id: %s, type %d\n", head->id, head->type);
        head = head->prev;
    }
}

void testSymTable(void) {
    int v1, v2, v3;
    // Create three symbols
    v1 = stab_setv("Symbol1", 'n', 0);
    v2 = stab_setv("Symbol2", 'n', 1);
    v3 = stab_setv("Symbol3", 'n', 2);
    printf("Test1: [%d][%d][%d]\n", v1, v2, v3);
    // Attempt to create three previously existing entries (should fail)
    v1 = stab_setv("Symbol1", 'n', 3);
    v2 = stab_setv("Symbol2", 'n', 3);
    v3 = stab_setv("Symbol3", 'n', 3);
    printf("Test2: [%d][%d][%d]\n", v1, v2, v3);
    // Set values in the three table entries
    v1 = stab_setv("Symbol1", 'v', 42);
    v2 = stab_setv("Symbol2", 'v', 11);
    v3 = stab_setv("Symbol3", 'v', 12);
    printf("Test3: [%d][%d][%d]\n", v1, v2, v3);
    // Get values from the three table entries
    v1 = stab_getv("Symbol1", 'v');
    v2 = stab_getv("Symbol2", 'v');
    v3 = stab_getv("Symbol3", 'v');
    printf("Test4: [%d][%d][%d]\n", v1, v2, v3);
    // Get an unset value from table entries, and a value from an not exist entry
    v1 = stab_getv("Symbol1", 't');
    v2 = stab_getv("Symbol1", 'n');
    v3 = stab_getv("NoSymbol", 'n');
    printf("Test5: [%d][%d][%d]\n", v1, v2, v3);
    stab_display();
}
