#ifndef __VM_H
#include <stdio.h>
#include <stdlib.h>
#define MAX_STACK_HEIGHT 2000
#define MAX_CODE_LENGTH 500
#define MAX_LEXI_LEVELS 10

typedef enum MathematicalOperations {
    RET = 0,
    NEG = 1,
    ADD = 2,
    SUB = 3,
    MUL = 4,
    DIV = 5,
    ODD = 6,
    MOD = 7,
    EQL = 8,
    NEQ = 9,
    LSS = 10,
    LEQ = 11,
    GTR = 12,
    GEQ = 13
} vm_opr;


typedef enum vm_opcode {
    NOP = 0,
    LIT = 1,
    OPR = 2,
    LOD = 3,
    STO = 4,
    CAL = 5,
    INC = 6,
    JMP = 7,
    JPC = 8,
    SI1 = 9,
    SI2 = 10
} vm_opcode;

typedef struct pcode {
    vm_opcode op;
    int l;
    vm_opr m;
    int cx;
    struct pcode *next;
    struct pcode *prev;
} pcode;


typedef struct SingleInstruction {
    int op;
    int l;
    int m;
} instruction;

typedef struct VM_Registers {
    int sp;
    int bp;
    int pc;
    instruction *ir;
} registers;

extern char *opcodeNums[];
extern FILE *vmoutfile;
extern FILE *vminfile;
//extern instruction tseg[MAX_CODE_LENGTH];
//extern registers regs;

void vm_init(void);
void readTextSegment(char *filename);
void fetch(void);
int exec(void);
void vm_run(void);
void vm_close(void);
void vm_runic(pcode *code);

#define __VM_H
#endif
