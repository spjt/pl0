#define DEBUG
#ifndef __COMMON_H
#define __COMMON_H
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>

FILE *openFile(char *filename, char *mode);
void *smallocf(size_t size, int line, const char function[]);
void *scallocf(size_t num, size_t size, int line, const char function[]);
void *sreallocf(void *ptr, size_t size, int line, const char function[]);

#define smalloc(size) smallocf(size,__LINE__,__func__)
#define scalloc(num, size) scallocf(num,size,__LINE__,__func__)
#define srealloc(ptr, size) sreallocf(ptr,size,__LINE__,__func__)

#ifdef DEBUG
#define d(...){fprintf(stdout,"(%s:%d): ",__func__,__LINE__);fprintf(stdout,__VA_ARGS__);fprintf(stdout,"\n");fflush(stdout);}
#define dn(...){fprintf(stdout,__VA_ARGS__);fflush(stdout);}
#define d2(...){fprintf(stdout,"(%s:%d): ",__func__,__LINE__);fprintf(stdout,__VA_ARGS__);fprintf(stdout,"\n");fflush(stdout);}
#else
#define d(...) {}
#define dn(...) {}
#define d2(...) {}
#endif /* DEBUG */
#endif /* __COMMON_H */

