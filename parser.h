#ifndef __PARSER_H
#include "scanner.h"
#define __PARSER_H

typedef struct symtable {
    int kind;       // const = 1, int = 2, proc = 3
    char name[MAXIDENT + PAD];  // name up to 11 chars
    int val;        // number (ASCII value)
    int level;      // L level
    int addr;       // M address
    struct symtable *next;
} symtable;

#define MAX_SYMBOL_TABLE_SIZE 1024

// Symbol table functions
void stab_free(symtable *head);
symtable *stab_gett(char *name);
/*  stab_setv
    Create/modify a symbol table entry.
    what = 'n'  : name
         = 'k'  : kind (1=const 2=int 3=proc)
         = 'v'  : val
         = 'l'  : level
         = 'a'  : addr

    return: 1 if successful, 0 if failure.
*/
int stab_setv(char *name, char what, int sett);
int stab_getv(char *name, char what);
void stab_display(void);
symtable *stab_getglob(void);

// Parser utility functions
int pars_etok(token_type ttype);
symtable *pars_init(lexemeListNode *head);
void pars_in(char *name);
void pars_error(int error);
void pars_gtok(void);

// RDP Functions
void pars_program(void);
void pars_block(void);
void pars_statement(void);
void pars_condition(void);
void pars_expression(void);
void pars_term(void);
void pars_factor(void);

#endif // __PARSER_H
