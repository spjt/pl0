#include "parser.h"
#include "scanner.h"
#include "common.h"
#include "vm.h"
#include "icg.h"
#include <stdio.h>
#include <stdlib.h>

lexemeListNode *pars_ctok = NULL; // current token
lexemeListNode *pars_htok = NULL; // head token
int pars_gtok_first = 1;
int stab_locnum = 0;
int stab_sbase = 4;
int stab_lexlev = 0;
static symtable  *symt_head = NULL;

const char *pars_errors[] = {
    "",
    " 1: Use = instead of :=",
    " 2: = must be followed by a number",
    " 3: Identifier must be followed by =",
    " 4: const, int, procedure must be followed by identifier",
    " 5: Semicolon or comma missing",
    " 6: Incorrect symbol after procedure declaration",
    " 7: Statement expected",
    " 8: Incorrect symbol after statement part in block",
    " 9: Period expected",
    "10: Semicolon between statements missing",
    "11: Undeclared identifier",
    "12: Assignment to constant or procedure is not allowed",
    "13: Assignment operator expected",
    "14: call must be followed by an identifier",
    "15: Call of a constant or variable is meaningless",
    "16: then expected",
    "17: Semicolon or } expected",
    "18: do expected",
    "19: Incorrect symbol following statement",
    "20: Relational operator expected",
    "21: Expression must not contain a procedure identifier",
    "22: Right parenthesis missing",
    "23: The preceding factor cannot begin with this symbol",
    "24: An expression cannot begin with this symbol",
    "25: This number is too large",
    "26: EOF before program end",
    "27: Redeclaration of const or var",
    "28: out/write must be followed by identifier"
};

char pars_current_context[30] = "";


// free allocated memory for symbol table
void stab_free(symtable *head) {
    if(head != NULL) {
        stab_free(head->next);
        free(head);
    }
}

// get symbol table entry for name. NULL if not exists.
symtable *stab_gett(char *name) {
    symtable *cur = NULL;
    cur = symt_head;
    while(cur != NULL) {
        if(strcmp(cur->name, name) == 0) {
            break;
        }
        cur = cur->next;
    }
    if(cur == NULL) {
        //d("symbol [%s] not found",name);
    }
    return cur;
}

/*
    Create/modify a symbol table entry.
    what = 'n'  : name
         = 'k'  : kind (1=const 2=int 3=proc)
         = 'v'  : val
         = 'l'  : level
         = 'a'  : addr

    return: 1 if successful, 0 if failure.
*/
int stab_setv(char *name, char what, int sett) {
    symtable *cur = NULL;
    symtable *new = NULL;
    cur = symt_head;
    // n: create a new symbol table entry: prepend to list.
    if(what == 'n') {
        // check list for name, if it already exists return 0.
        cur = stab_gett(name);
        if(cur != NULL) {
            //d("error: adding symbol [%s] already in table", name);
            return 0;
        }
        // Didn't find it, prepend to list.
        // Fill in default values. Note that default kind is 0 (not a valid kind)
        // This way we can see if it's been set properly.
        new = (symtable *)smalloc(sizeof(symtable));
        strcpy(new->name, name);
        new->kind = 0;
        new->val = 0;
        new->level = sett;
        new->addr = 0;
        new->next = symt_head;
        symt_head = new;
        return 1;
    }
    // otherwise set a value for a preexisting name.
    else {
        cur = stab_gett(name);
        if(cur == NULL) {
            return 0;    // error: no symbol name
        }
        else {
            switch(what) {
            case 'k':
                cur->kind = sett;
                break;
            case 'v':
                cur->val = sett;
                break;
            case 'l':
                cur->level = sett;
                break;
            case 'a':
                cur->addr = sett;
                break;
            default:
                return 0; //error: unknown what
            }
        }
        return 1;
    }
}
/*
    Get a symbol table entry
    what = 'n'  : name (1 if exists, 0 if not exists)
         = 'k'  : kind (1=const 2=int 3=proc)
         = 'v'  : val
         = 'l'  : level
         = 'a'  : addr
*/
int stab_getv(char *name, char what) {
    symtable *cur = NULL;
    cur = stab_gett(name);
    if(cur==NULL && what != 'n') pars_error(8);
    switch(what) {
    case 'n':
        if(cur == NULL) {
            return 0;
        }
        else {
            return 1;
        }
        break;
    case 'k':
        return cur->kind;
        break;
    case 'v':
        return cur->val;
        break;
    case 'l':
        return cur->level;
        break;
    case 'a':
        return cur->addr;
        break;
    default:
        return -1;
    }
}
symtable *stab_getglob(void) {
    return symt_head;
}
void stab_display() {
    symtable *cur = symt_head;
    printf("Symbol Table:\n");
    printf("    %-15s%-6s%-6s%-6s%-6s\n", "name", "kind", "val", "level", "addr");
    while(cur != NULL) {
        printf("    %-15s%-6d%-6d%-6d%-6d\n", cur->name, cur->kind, cur->val, cur->level, cur->addr);
        cur = cur->next;
    }
}
void pars_in(char *name) {
    strcpy(pars_current_context, name);
}

// print an error from the predefined error strings
void pars_error(int error) {
    printf("\nParse error %s at id [", pars_errors[error]);
    if(pars_ctok->id == NULL) {
        printf("<EOF>");
    }
    else {
        printf("%s",pars_ctok->id);
    }
    printf("]\n");
    exit(1);
}

// return if pars_ctok is ttype
int pars_etok(token_type ttype) {
    if(pars_ctok == NULL) {
        return 0;
    }
    else {
        return (pars_ctok->type == ttype);
    }
}

// Set the current token to the next token.
void pars_gtok(void) {
    if(pars_gtok_first == 1) {
        //printf("in %s: %s \n",pars_current_context,pars_ctok->id);
        pars_gtok_first = 0;
        return;
    }
    if(pars_ctok == NULL) {
        pars_error(8);
    }
    if(pars_ctok->next != NULL) {
        pars_ctok = pars_ctok->next;
        //printf("in %s: %s \n",pars_current_context,pars_ctok->id);
        fflush(stdout);
    }
    else {
        pars_ctok = NULL;
    }
}

symtable *pars_init(lexemeListNode *head) {
    pars_ctok = head;
    pars_htok = head;
    pars_program();
    return symt_head;
}

//---------------------------------------------------------[Process Grammar]
void pars_program(void) {
    icg_add(INC,0,4);
    pars_in("program");
    pars_gtok(); 
    pars_block();
    if(!pars_etok(periodsym)) {
        pars_error(9);
    }
}

void pars_block(void) {
    // d("start block");
    char tempid[MAXIDENT + PAD];
    pcode *tempcode = NULL;
    pars_in("block");
    if(pars_etok(constsym)) {
        do {
            pars_gtok();
            if(!pars_etok(identsym)) {
                pars_error(4);
            }
            if(stab_gett(pars_ctok->id) != NULL) {
                //redefinition of const
                pars_error(27);
            }
            strcpy(tempid, pars_ctok->id);
            stab_setv(tempid, 'n', 0);
            stab_setv(tempid, 'k', 1);
            pars_gtok();
            if(!pars_etok(eqsym)) {
                pars_error(3);
            }
            pars_gtok();
            if(!pars_etok(numbersym)) {
                pars_error(2);
            }
            stab_setv(tempid, 'v', atoi(pars_ctok->id));
            pars_gtok();
        }
        while(pars_etok(commasym));
        if(!pars_etok(semicolonsym)) {
            pars_error(5);
        }
        pars_gtok();
    }
    if(pars_etok(intsym)) {
        do {
            pars_gtok();
            if(!pars_etok(identsym)) {
                pars_error(4);
            }
            // Entry in symbol table
            strcpy(tempid, pars_ctok->id);
            if(stab_gett(pars_ctok->id) != NULL && stab_getv(pars_ctok->id, 'l') == stab_lexlev) {
                pars_error(27); //redeclaration
            }
            stab_setv(tempid, 'n', stab_lexlev);
            stab_setv(tempid, 'k', 2);

            if (stab_getv(pars_ctok->id, 'l') > stab_lexlev) {
                // only set lexlev if this one is lower
                stab_setv(tempid, 'l', stab_lexlev);
            }
            stab_setv(tempid, 'a', stab_sbase + (stab_locnum++));
            stab_setv(tempid, 'v', 0);
            // Emit code (increment SP)
            icg_add(INC, 0, 1);
            pars_gtok();
        }
        while(pars_etok(commasym));
        if(!pars_etok(semicolonsym)) {
            pars_error(5);
        }
        pars_gtok();
    }
    while(pars_etok(procsym)) {
        pars_gtok();
        if(!pars_etok(identsym)) {
            pars_error(6);
        }
        stab_locnum = 0;
        icg_add(JMP,0,0);
        tempcode = icg_last();
        strcpy(tempid, pars_ctok->id);
        stab_setv(tempid, 'n', 0);
        stab_setv(tempid, 'k', 3);
        stab_setv(tempid, 'a', icg_cx());
        stab_setv(tempid, 'l', stab_lexlev++);
        stab_setv(tempid, 'v', 0);
        icg_add(INC,0,4);
        pars_gtok();
        if(!pars_etok(semicolonsym)) {
            pars_error(5);
        }
        pars_gtok();
        pars_block();
        icg_add(OPR,0,0);
        tempcode->m = icg_cx();
        stab_lexlev--;

        if(!pars_etok(semicolonsym)) {
            pars_error(5);
        }
        pars_gtok();
    }
    // This wasn't part of the grammar but it seems like it should be.
    //if(pars_etok(intsym) || pars_etok(constsym) || pars_etok(procsym)) {
    //    pars_block();
    //}
    pars_statement();
    // d("end block");
}

void pars_statement(void) {
    char tempid[MAXIDENT + PAD];
    pcode *tempcode = NULL;
    pcode *tempcode2 = NULL;
    int cx1, cs2;
    pars_in("statement");
    //d("start statement at %s",pars_ctok->id);
    if(pars_etok(identsym)) {
        strcpy(tempid, pars_ctok->id);
        if(stab_getv(tempid, 'k') != 2) {
            pars_error(12);
        }
        pars_gtok();
        if(!pars_etok(becomessym)) {
            pars_error(13);
        }
        pars_gtok();
        pars_expression();
        icg_add(STO, stab_lexlev-stab_getv(tempid, 'l'), stab_getv(tempid, 'a'));
    }
    else if(pars_etok(callsym)) {
        pars_gtok();
        if(!pars_etok(identsym)) {
            pars_error(14);
        }
        if(stab_getv(pars_ctok->id, 'k') != 3) {
            pars_error(15);
        }
        icg_add(CAL, stab_getv(pars_ctok->id, 'l'), stab_getv(pars_ctok->id, 'a'));
        pars_gtok();
    }
    else if(pars_etok(beginsym)) {
        do {
            pars_gtok();
            pars_statement();
        } while(pars_etok(semicolonsym));

        if(!pars_etok(endsym)) {
            pars_error(26);
        }
        pars_gtok();
    }

    else if(pars_etok(ifsym)) {
        pars_gtok();
        pars_condition();
        if(!pars_etok(thensym)) {
            pars_error(16);
        }
        pars_gtok();
        icg_add(JPC, 0, 0);
        tempcode = icg_last();
        pars_statement();


        if(pars_etok(elsesym)) {
            icg_add(JMP, 0, 0); // else
            tempcode2 = icg_last();
            tempcode->m = icg_cx();
            pars_gtok();
            pars_statement();
            tempcode2->m = icg_cx();
        }
        else tempcode->m = icg_cx();
    }
    else if(pars_etok(whilesym)) {
        cx1 = icg_cx();
        pars_gtok();
        pars_condition();
        icg_add(JPC, 0, 0);
        tempcode = icg_last();
        if(!pars_etok(dosym)) {
            pars_error(18);
        }
        pars_gtok();
        pars_statement();
        icg_add(JMP, 0, cx1);
        tempcode->m = icg_cx();
    }
    else if(pars_etok(insym)) {
        pars_gtok();
        if(!pars_etok(identsym)) {
            pars_error(28);
        }
        icg_add(SI2,0,2);
        icg_add(STO,stab_getv(pars_ctok->id, 'l'), stab_getv(pars_ctok->id, 'a'));
        pars_gtok();
    }
    else if(pars_etok(outsym)) {
        pars_gtok();
        pars_expression();
        icg_add(SI1,0,1);
    }
    //d("end statement at %s",pars_ctok->id);
}

void pars_condition(void) {
    token_type temptok;
    pars_in("condition");
    if(pars_etok(oddsym)) {
        pars_gtok();
        pars_expression();
        icg_add(OPR, 0, ODD);
    }
    else {
        pars_expression();
        // check for all relational symbols.
        if(pars_etok(becomessym)) {
            pars_error(1);
        }
        if( !pars_etok(eqsym)  &&
                !pars_etok(neqsym) &&
                !pars_etok(lessym) &&
                !pars_etok(leqsym) &&
                !pars_etok(gtrsym) &&
                !pars_etok(geqsym)) {
            pars_error(20);
        }
        temptok = pars_ctok->type;
        pars_gtok();
        pars_expression();
        switch(temptok) {
        case eqsym  :
            icg_add(OPR, 0, EQL);
            break;
        case neqsym :
            icg_add(OPR, 0, NEQ);
            break;
        case lessym :
            icg_add(OPR, 0, LSS);
            break;
        case leqsym :
            icg_add(OPR, 0, LEQ);
            break;
        case gtrsym :
            icg_add(OPR, 0, GTR);
            break;
        case geqsym :
            icg_add(OPR, 0, GEQ);
            break;
        }
    }
}

void pars_expression(void) {
    token_type temptok;
    pars_in("expression");
    // negation in front of symbol
    if(pars_etok(plussym) || pars_etok(minussym)) {
        temptok = pars_ctok->type;
        pars_gtok();
        pars_term();
        if(temptok == minussym) {
            icg_add(OPR, 0, NEG);
        }
    }
    else {
        pars_term();
    }
    while(pars_etok(plussym) || pars_etok(minussym)) {
        temptok = pars_ctok->type;
        pars_gtok();
        pars_term();
        if(temptok == plussym) {
            icg_add(OPR, 0, ADD);
        }
        if(temptok == minussym) {
            icg_add(OPR, 0, SUB);
        }
    }
}

void pars_term(void) {
    token_type temptok;
    pars_in("term");
    pars_factor();
    while(pars_etok(multsym) || pars_etok(slashsym)) {
        temptok = pars_ctok->type;
        pars_gtok();
        pars_factor();
        if(temptok == multsym) {
            icg_add(OPR, 0, MUL);
        }
        if(temptok == slashsym) {
            icg_add(OPR, 0, DIV);
        }
    }
}

void pars_factor(void) {
    pars_in("factor");
    if(pars_etok(identsym)) {
        if(stab_gett(pars_ctok->id) == NULL) {
            pars_error(11);
        }
        switch (stab_getv(pars_ctok->id, 'k')) {
        case 1:
            icg_add(LIT, 0, stab_getv(pars_ctok->id, 'v'));
            break;
        case 2:
            icg_add(LOD, stab_lexlev-stab_getv(pars_ctok->id, 'l'), stab_getv(pars_ctok->id, 'a'));
            break;
        case 3:
            pars_error(21);
        }
        pars_gtok();
    }
    else if(pars_etok(numbersym)) {
        icg_add(LIT, 0, atoi(pars_ctok->id));
        pars_gtok();
    }
    else if (pars_etok(lparentsym)) {
        pars_gtok();
        pars_expression();
        if(!pars_etok(rparentsym)) {
            pars_error(22);
        }
        pars_gtok();
    }
    else {
        pars_error(23);
    }
}








