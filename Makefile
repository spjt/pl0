EXE = hw4
OBJS = hw4.o icg.o vm.o scanner.o common.o parser.o

CC = gcc
CFLAGS = -g 
LIBS = -lm
LD = gcc
LDFLAGS = $(CFLAGS)

all: $(EXE)

dist: $(EXE) dclean

dclean:
	rm -f $(OBJS) core *~

$(EXE): $(OBJS)
	$(CC) $(LDFLAGS) $(OBJS) -o $(EXE)

clean: dclean
	rm -f $(EXE)
