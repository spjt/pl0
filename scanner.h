#ifndef __SCANNER_H
#include <stdio.h>
#define __SCANNER_H
#define MAXIDENT 11
#define MAXNUMDIG 5
#define HASHPRIME 1009
#define BUFF 1024
#define PAD 2

typedef enum token_type {
    nulsym = 1, identsym = 2, numbersym = 3, plussym = 4, minussym = 5,
    multsym = 6,  slashsym = 7, oddsym = 8, eqsym = 9, neqsym = 10, lessym = 11, leqsym = 12,
    gtrsym = 13, geqsym = 14, lparentsym = 15, rparentsym = 16, commasym = 17, semicolonsym = 18,
    periodsym = 19, becomessym = 20, beginsym = 21, endsym = 22, ifsym = 23, thensym = 24,
    whilesym = 25, dosym = 26, callsym = 27, constsym = 28, intsym = 29, procsym = 30, outsym = 31,
    insym = 32, elsesym = 33
} token_type;

typedef struct lexemeListNode {
    char id[MAXIDENT + PAD];
    token_type type;
    struct lexemeListNode *next;
    struct lexemeListNode *prev;
} lexemeListNode;

lexemeListNode *getLexemeList(FILE *input);
void runScanner(FILE *input, FILE *output, lexemeListNode *lexemeListHead, lexemeListNode *lexemeTableHead);
lexemeListNode *createLexemeTable(FILE *input, FILE *output, lexemeListNode *lexemeTableHead);
void grabNextToken(FILE *input, char *token);
lexemeListNode *createLexemeList(FILE *input, FILE *output, lexemeListNode *lexemeTableHead);
void freeLexemeList(lexemeListNode *head);

void dumpLexemeList(FILE *output, lexemeListNode *head);
void dumpLexemeTable(FILE *output, lexemeListNode *head);
token_type tokenToValue(char *token);

#endif // __SCANNER_H
