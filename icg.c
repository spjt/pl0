#include "icg.h"
#include "vm.h"
#include "common.h"
static pcode *icg_lhead = NULL;
static pcode *icg_ltail = NULL;
lexemeListNode *icg_ll;
symtable *icg_st;

pcode *icg_add(vm_opcode op, int l, vm_opr m) {
    pcode *new;
    new = (pcode *)smalloc(sizeof(pcode));
    new->op = op;
    new->l = l;
    new->m = m;
    new->next = NULL;
    new->prev = NULL;
    // create a new list
    if(icg_lhead == NULL) {
        new->cx = 0;
        icg_lhead = new;
        icg_ltail = new;
    }
    else { // append to list
        new->cx = icg_ltail->cx + 1;
        icg_ltail->next = new;
        new->prev = icg_ltail;
        icg_ltail = new;
    }
    return new;
}

void icg_free(pcode *head) {
    if(head != NULL) {
        icg_free(head->next);
        free(head);
    }
}

void icg_display(void) {
    extern char *opcodeNums[];
    extern char *oprNums[];
    pcode *cur = icg_lhead;
    printf("Intermediate Code:\n");
    printf("    %-5s%-5s%-5s%-5s%-5s\n", "#", "code", "op", "l", "m");
    while(cur != NULL) {
        if(cur->op != 2) {
            printf("    %-5d%-5s%-5d%-5d%-5d\n", cur->cx, opcodeNums[cur->op], cur->op, cur->l, cur->m);
        }
        else {
            printf("    %-5d%-5s%-5d%-5d%-5d (%s)\n", cur->cx, opcodeNums[cur->op], cur->op, cur->l, cur->m, oprNums[cur->m]);
        }
        cur = cur->next;
    }
}

// returns the NEXT cx (current +1)
int icg_cx(void) {
    if(icg_ltail != NULL) {
        return icg_ltail->cx + 1;
    }
    else {
        return 0;
    }
}

pcode *icg_last(void) {
    return icg_ltail;
}

pcode *icg_init(lexemeListNode *lexemeList, symtable *symbols) {
    //icg_test();
    icg_ll = lexemeList;
    icg_st = symbols;
    return icg_lhead;
}

// generated code from hw1
pcode *icg_test(void) {
    icg_add(7, 0, 10);
    icg_add(7, 0, 2 );
    icg_add(6, 0, 6 );
    icg_add(1, 0, 13);
    icg_add(4, 0, 4 );
    icg_add(1, 0, 1 );
    icg_add(4, 1, 4 );
    icg_add(1, 0, 7 );
    icg_add(4, 0, 5 );
    icg_add(2, 0, 0 );
    icg_add(6, 0, 6 );
    icg_add(1, 0, 3 );
    icg_add(4, 0, 4 );
    icg_add(1, 0, 0 );
    icg_add(4, 0, 5 );
    icg_add(5, 0, 2 );
    icg_add(2, 0, 0 );
    return icg_lhead;
}

void icg_process(void) {
}
