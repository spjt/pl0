To compile:

make clean; make

to run: 

./hw4 [arguments] <inputfile>

Arguments:
	-l: Print the list of lexemes/tokens (scanner output) to the screen
	-a: Print the generated assembly code (parser/codegen output) to the screen
	-v: Print virtual machine execution trace (virtual machine output) to the screen

running ./hw4 with no arguments will output the above help.

-----------------
Valid PL0 program
There are some sample programs located in 'sample'. Output is provided for
recurse.pl0 in the file recurse.out.

-----------------
Errors
The files demonstrating parser errors are located in 'errors'.
You can generate them all with the 'gentxt' script there. 
The output of that script is in errors/errors.txt.

