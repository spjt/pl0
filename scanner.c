
/*
 * 

 *
 * 
 * 
 *
 */


#include "common.h"
#include "scanner.h"

const char *symbolnames[] = {
    "ERROR", "nul", "ident", "number", "plus", "minus",
    "mult", "slash", "odd", "eq", "neq", "les", "leq",
    "gtr", "geq", "lparent", "rparent", "comma", "semicolon",
    "period", "becomes", "begin", "end", "if", "then",
    "while", "do", "call", "const", "int", "proc", "out",
    "in", "else"
};

void runScanner(FILE *input, FILE *output, lexemeListNode *lexemeListHead, lexemeListNode *lexemeTableHead) {
    char buf[BUFF + PAD];
    puts("Source Program:\n");
    // Display the input file.
    while(!feof(input)) {
        fgets(buf, BUFF, input);
        if(!feof(input)) {
            fputs(buf, output);
        }
    }
    rewind(input);
    fputs("\n", output);
    fflush(output);
    // Create the lexeme table.
    while(!feof(input)) {
        lexemeTableHead = createLexemeTable(input, output, lexemeTableHead);
    }
    rewind(input);
    dumpLexemeTable(output, lexemeTableHead);
    fputs("\n", output);
    fflush(output);
    // Create the lexeme list.
    /*
    while(!feof(input)) {
      lexemeListHead = createLexemeList(input,lexemeTableHead);
    }
    */
    while (!feof(input)) {
        createLexemeList(input, output, lexemeTableHead);
    }
    rewind(input);
    dumpLexemeList(output, lexemeListHead);
    fputs("\n", output);
    fflush(output);
}

void dumpLexemeList(FILE *output, lexemeListNode *head) {
}
void dumpLexemeTable(FILE *output, lexemeListNode *head) {
}

lexemeListNode *createLexemeList(FILE *input, FILE *output, lexemeListNode *lexemeTableHead) {
    char token[MAXIDENT + PAD];
    token_type value = 0;
    fprintf(output, "Lexeme List:\n");
    while(!feof(input)) {
        grabNextToken(input, token);
        value = tokenToValue(token);
        if(!feof(input)) {
            if(value == identsym || value == numbersym) {
                fprintf(output, "%d %s ", value, token);
            }
            else {
                fprintf(output, "%d ", value);
            }
        }
    }
    fprintf(output, "\n");
}



lexemeListNode *createLexemeTable(FILE *input, FILE *output, lexemeListNode *lexemeTableHead) {
    lexemeListNode *lexemeTableTail = NULL;
    char token[MAXIDENT + PAD];
    token_type value = 0;
    fprintf(output, "Lexeme Table:\n");
    fprintf(output, "%-20s%-20s%-20s\n", "lexeme", "token type", "token name");
    while(!feof(input)) {
        grabNextToken(input, token);
        value = tokenToValue(token);
        if(!feof(input)) {
            fprintf(output, "%-20s%-20d%-20s\n", token, value, symbolnames[value]);
        }
    }
}

lexemeListNode *getLexemeList(FILE *input) {
    lexemeListNode *head = NULL;
    lexemeListNode *new = NULL;
    lexemeListNode *cur = NULL;
    char token[MAXIDENT + PAD];
    token_type value = 0;
    while(!feof(input)) {
        grabNextToken(input, token);
        value = tokenToValue(token);
        if(!feof(input)) {
            new = (lexemeListNode *)smalloc(sizeof(lexemeListNode));
            // start a new list
            if(head == NULL) {
                head = new;
            }
            strcpy(new->id, token);
            new->type = value;
            new->prev = cur;
            if(cur != NULL) {
                cur->next = new;
            }
            cur = new;
        }
    }
    return head;
}

void freeLexemeList(lexemeListNode *head) {
    if(head != NULL) {
        freeLexemeList(head->next);
        free(head);
    }
}

token_type tokenToValue(char *token) {
    if(strcmp(token, "null") == 0) {
        return nulsym;
    }
    if(strcmp(token, "const") == 0) {
        return constsym;
    }
    else if(strcmp(token, "int") == 0) {
        return intsym;
    }
    else if(strcmp(token, "procedure") == 0) {
        return procsym;
    }
    else if(strcmp(token, "call") == 0) {
        return callsym;
    }
    else if(strcmp(token, "begin") == 0) {
        return beginsym;
    }
    else if(strcmp(token, "end") == 0) {
        return endsym;
    }
    else if(strcmp(token, "if") == 0) {
        return ifsym;
    }
    else if(strcmp(token, "then") == 0) {
        return thensym;
    }
    else if(strcmp(token, "else") == 0) {
        return elsesym;
    }
    else if(strcmp(token, "while") == 0) {
        return whilesym;
    }
    else if(strcmp(token, "do") == 0) {
        return dosym;
    }
    else if(strcmp(token, "in") == 0) {
        return insym;
    }
    else if(strcmp(token, "out") == 0) {
        return outsym;
    }
    else if(strcmp(token, "read") == 0) {
        return insym;
    }
    else if(strcmp(token, "write") == 0) {
        return outsym;
    }
    else if(strcmp(token, "odd") == 0) {
        return oddsym;
    }
    else if(strcmp(token, "+") == 0) {
        return plussym;
    }
    else if(strcmp(token, "-") == 0) {
        return minussym;
    }
    else if(strcmp(token, "*") == 0) {
        return multsym;
    }
    else if(strcmp(token, "/") == 0) {
        return slashsym;
    }
    else if(strcmp(token, "=") == 0) {
        return eqsym;
    }
    else if(strcmp(token, "<>") == 0) {
        return neqsym;
    }
    else if(strcmp(token, "<") == 0) {
        return lessym;
    }
    else if(strcmp(token, "<=") == 0) {
        return leqsym;
    }
    else if(strcmp(token, ">") == 0) {
        return gtrsym;
    }
    else if(strcmp(token, ">=") == 0) {
        return geqsym;
    }
    else if(strcmp(token, "(") == 0) {
        return lparentsym;
    }
    else if(strcmp(token, ")") == 0) {
        return rparentsym;
    }
    else if(strcmp(token, ",") == 0) {
        return commasym;
    }
    else if(strcmp(token, ";") == 0) {
        return semicolonsym;
    }
    else if(strcmp(token, ".") == 0) {
        return periodsym;
    }
    else if(strcmp(token, ":=") == 0) {
        return becomessym;
    }
    else if((token[0] >= 'A' && token[0] <= 'Z') ||
            (token[0] >= 'a' && token[0] <= 'z')) {
        return identsym;
    }
    else if(token[0] >= '0' || token[0] <= '9') {
        return numbersym;
    }
}

void grabNextToken(FILE *input, char *token) {
    char readchar;
    int readctr = 0; // count number of characters in token
    int commentEnd = 0; // determine end of comment
    token[0] = '\0';
    readchar = fgetc(input);
    //deal with whitespace
    if(readchar == ' ' || readchar == '\t' || readchar == '\r' ||
            readchar == '\n') {
        grabNextToken(input, token);
        return;
    }
    //deal with simple special symbols
    if(readchar == '+' || readchar == '-' || readchar == '*' ||
            readchar == '(' || readchar == ')' || readchar == '=' ||
            readchar == ',' || readchar == '.' || readchar == ';') {
        token[0] = readchar;
        token[1] = '\0';
        return;
    }
    // deal with < <= <>
    if(readchar == '<') {
        readchar = fgetc(input);
        token[0] = '<';
        if(readchar == '=' || readchar == '>') {
            token[1] = readchar;
            token[2] = '\0';
            return;
        }
        else {
            ungetc(readchar, input);
            token[1] = '\0';
            return;
        }
    }
    // deal with > >=
    if(readchar == '>') {
        readchar = fgetc(input);
        token[0] = '>';
        if(readchar == '=') {
            token[1] = readchar;
            token[2] = '\0';
            return;
        }
        else {
            ungetc(readchar, input);
            token[1] = '\0';
            return;
        }
    }
    // deal with :=
    if(readchar == ':') {
        readchar = fgetc(input);
        if(readchar == '=') {
            strcpy(token, ":=");
            return;
        }
        else {
            fprintf(stderr, "invalid symbol, : not part of :=\n");
            exit(1);
        }
    }
    //deal with /
    if(readchar == '/') {
        readchar = fgetc(input);
        if(readchar == '*') { // start of comment
            while(commentEnd != 1) {
                readchar = fgetc(input);
                if(readchar == '*') {
                    readchar = fgetc(input);
                    if(readchar == '/') { // end of comment
                        commentEnd = 1;
                    }
                }
            }
            grabNextToken(input, token); // call again
            return;
        }
        else {
            token[0] = '/';
            token[1] = '\0';
            ungetc(readchar, input);
            return;
        }
    }
    // deal with numbers
    if(readchar >= '0' && readchar <= '9') {
        token[0] = readchar;
        do {
            readchar = fgetc(input);
            readctr++;
            if(readctr > MAXNUMDIG) {
                token[readctr] = '\0';
                fprintf(stderr, "Number too long: %s...\n", token);
                exit(0);
            }
            if((readchar >= 'a' && readchar <= 'z') ||
                    (readchar >= 'A' && readchar <= 'Z')) {
                fprintf(stderr, "Variable does not start with letter.\n");
                exit(0);
            }
            if(readchar >= '0' && readchar <= '9') {
                token[readctr] = readchar;
            }
            else {
                token[readctr] = '\0'; //terminate string
                ungetc(readchar, input);
                return;
            }
        }
        while(1);
        return;
    }
    // identifiers
    if( (readchar >= 'a' && readchar <= 'z') ||
            (readchar >= 'A' && readchar <= 'Z') ) {
        token[0] = readchar;
        do {
            readchar = fgetc(input);
            readctr++;
            if(readctr > MAXIDENT) {
                fprintf(stderr, "Name too long: %s...\n", token);
                exit(1);
            }
            if((readchar >= 'a' && readchar <= 'z') ||
                    (readchar >= 'A' && readchar <= 'Z') ||
                    (readchar >= '0' && readchar <= '9')) {
                token[readctr] = readchar;
            }
            else {
                token[readctr] = '\0';
                ungetc(readchar, input);
                return;
            }
        }
        while (1);
    }
    if(feof(input)) {
        return;
    }
    fprintf(stderr, "Invalid symbol: [%c] (ASCII %d)\n", readchar, readchar);
    exit(1);
}


