#ifndef __ICG_H
#define __ICG_H
#include <stdio.h>
#include <stdlib.h>
#include "scanner.h"
#include "parser.h"
#include "vm.h"


pcode *icg_add(vm_opcode op, int l, vm_opr m);
pcode *icg_init(lexemeListNode *lexemeList, symtable *symbols);
void icg_free(pcode *icg_lhead);
void icg_display(void);
pcode *icg_test(void);
int icg_cx(void);
pcode *icg_last(void);
#endif // __ICG_H
