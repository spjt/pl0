/* common.c
 * Miscellaneous boilerplate and helper functions - pjw
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <strings.h>
#include "common.h"

/* Open file, with helpful error messages if it doesn't work. */
FILE *openFile(char *filename, char *mode) {
    FILE *fptr;
    if ((fptr = fopen(filename, mode)) == NULL) { // fopen() returns null pointer on failure
        fprintf(stderr, "Failed to open input file [%s] with mode [%s]\n", filename, mode);
        exit(EXIT_FAILURE);
    }
    return fptr;
}

/* A malloc which prints error if it doesn't work. */
void *smallocf(size_t size, int line, const char function[]) {
    void *tmp;
    if ((tmp = malloc(size)) == NULL) { // malloc() returns NULL when allocation fails.
        fprintf(stderr, "Memory allocation failed at %s:%d.\n", function, line);
        exit(EXIT_FAILURE); //Quit.
    }
    return tmp;
}

void *scallocf(size_t num, size_t size, int line, const char function[]) {
    void *tmp;
    if ((tmp = calloc(num, size)) == NULL) {
        fprintf(stderr, "Memory allocation failed at %s:%d.\n", function, line);
        exit(EXIT_FAILURE);
    }
    return tmp;
}

void *sreallocf(void *ptr, size_t size, int line, const char function[]) {
    void *tmp;
    if ((tmp = realloc(ptr, size)) == NULL) {
        fprintf(stderr, "Memory allocation failed at %s:%d.\n", function, line);
    }
}

