/*
 * 

 *
 * 
 * 
 *
 */

/*
 * vm.c
 *  The internal operation of the Virtual Machine.
 *
 */

#include "vm.h"
#include "icg.h"
#include "common.h"

void status(void);
#ifdef DEBUG
void status(void) {
}
#else
void status(void) {}
#endif

void OP_LIT(void);
void OP_OPR(void);
void OP_LOD(void);
void OP_STO(void);
void OP_CAL(void);
void OP_INC(void);
void OP_JMP(void);
void OP_JPC(void);
void OP_SIO(void);

// Program scope vars
char *opcodeNums[] = {
    "NOP", "LIT", "OPR", "LOD", "STO", "CAL", "INC", "JMP", "JPC", "SI1", "SI2"
};

char *oprNums[] = {
    "RET", "NEG", "ADD", "SUB", "MUL", "DIV", "ODD", "MOD", "EQL", "NEQ", "LSS", "LEQ", "GTR", "GEQ"
};

// File scope vars
static int pLine; // line to print
static instruction tseg[MAX_CODE_LENGTH];
static registers regs;
static int stk[MAX_STACK_HEIGHT];
static int frames[MAX_LEXI_LEVELS + 1];
static int llev = 0;
static int debugbreak = 0;
// This reads the input file into the text segment tseg.
void readTextSegment(char *filename) {
    FILE *input;
    char buf[100];
    int i = 0, j = 0;
    input = openFile(filename, "r");
    // count number of lines in file
    while(!feof(input)) {
        fgets(buf, 100, input);
        // next line checks for a valid integer
        if(! (buf[0] < '0' || buf[0] > '9') ) {
            sscanf(buf, "%d%d%d", &tseg[i].op, &tseg[i].l, &tseg[i].m);
            i++;
        }
    }
    //d("Read %d lines",i);
    fprintf(vmoutfile, "Line  OP   L   M\n");
    for(j = 0; j < i; j++) {
        fprintf(vmoutfile, "%4d %s %3d %3d\n", j, opcodeNums[tseg[j].op], tseg[j].l, tseg[j].m);
    }
}

void vm_runic(pcode *code) {
    pcode *cur = code;
    int i = 0, j = 0;
    char *format = "%4d %s %3d %3d\n";
    vm_init();
    while (cur != NULL) {
        tseg[i].op = cur->op;
        tseg[i].l = cur->l;
        tseg[i].m = cur->m;
        i++;
        cur = cur->next;
    }
    vm_run();
}

// reads from the input file (for SIO 0,2) and returns an integer
int read(void) {
    char buf[100];
    int inp;
    fprintf(stdout,"\nINPUT >> ");
    fgets(buf, 100, stdin);
    sscanf(buf, "%d", &inp);
    return inp;
}

// prints to the screen (used by SIO 0,1)
void print(int n) {
    fprintf(stdout, "\nOUTPUT >> ");
    fprintf(stdout, "%d\n", n);
}

// Read the next instruction into the IR
void fetch(void) {
    regs.ir = &tseg[regs.pc];
    if(debugbreak++ > 200) {
        d("Debug break for exceeding max lines of output");
        exit(0);
    }
    /* d("INST: op(%s)=[%d] l=[%d] m=[%d]",
    opcodeNums[regs.ir->op],
    regs.ir->op,regs.ir->l,regs.ir->m); */
}

// Finds the base pointer L lexicographical levels down.
int rebase(l, base) {
    int b1, lin; //find base L levels down
    lin = l;
    b1 = base;
    while (lin > 0) {
        b1 = stk[b1+1]; //base ptr is now bp+1
        lin--;
    }
    //d("base(%d,%d) = %d",lin,base,b1);
    return b1;
}

// Finds the base pointer at the given level
int base (l, base) {
    if(rebase(l,base) == 0) return 1; // minimum base pointer
    return rebase(l, base);
}
// Print the current state of the virtual machine.
void printState() {
    int i, j = 0;
    char *opPrint;
    if(vmoutfile != NULL) {
        fprintf(vmoutfile, "%3d ", pLine);
        fprintf(vmoutfile, "%s %3d %3d     %3d %3d %3d     ",
            opcodeNums[regs.ir->op], regs.ir->l, regs.ir->m,
            regs.pc, regs.bp, regs.sp);

    //for(i = 1; i <= (regs.sp > regs.bp ? regs.sp : regs.bp + 3); i++) {
        for(i=1; i <= regs.sp; i++) {
            if((frames[j] == i)) {
                if (j != 0) fprintf(vmoutfile, "| ");
                j++;
            }
            fprintf(vmoutfile, "%d ", stk[i]);
        }
        fprintf(vmoutfile, "\n");
    }
}

// Execute (second half of fetch/execute cycle)
int exec(void) {
    pLine = regs.pc;
    //fprintf(vmoutfile,"%3d ",regs.pc);
    regs.pc++;
    switch(regs.ir->op) {
    case 0:
        return -1;
        break;
    case 1:
        OP_LIT();
        break;
    case 2:
        OP_OPR();
        break;
    case 3:
        OP_LOD();
        break;
    case 4:
        OP_STO();
        break;
    case 5:
        OP_CAL();
        break;
    case 6:
        OP_INC();
        break;
    case 7:
        OP_JMP();
        break;
    case 8:
        OP_JPC();
        break;
    case 9:
        OP_SIO();
        break;
    case 10:
        OP_SIO();
        break;
    }
    printState();
}


// Set up some initial conditions.
void vm_init(void) {
    int i;
    regs.sp = 0;
    regs.bp = 1;
    regs.pc = 0;
    regs.ir = NULL;
    frames[0] = 1;
    //d("sp = [%d] bp = [%d] pc = [%d] ir = [%p]",regs.sp,regs.bp,regs.pc,regs.ir);
    // zero out the stack.
    for(i = 0; i < MAX_STACK_HEIGHT; i++) {
        stk[i] = 0;
    }
}

// Start the virtual machine.
void vm_run(void) {
    int i;
    int halt = 0;
    if(vmoutfile != NULL) {
        fprintf(vmoutfile,
            " pc op   l   m       pc  bp  sp     stack\n");
        fprintf(vmoutfile,
            " initial values     %3d %3d %3d   %3d\n", regs.pc, regs.bp, regs.sp, stk[0]);
    }
    while(!halt) {
        fetch();
        if(regs.bp == 0 && regs.pc == 0) {
            halt = 1;
        }
        if(!halt)
            if(exec() == -1) {
                halt = 1;
            }
    }
}

// Clean up.
void vm_close(void) {
    if(vminfile != NULL) {
        fclose(vminfile);
    }
    if(vmoutfile != NULL) {
        fclose(vmoutfile);
    }
}


//*****************************************************************
// Implementation of the opcodes.
//*****************************************************************


void OP_LIT(void) {
    //d("regs.sp=%d, regs.ir->m=%d",regs.sp,regs.ir->m);
    regs.sp++;
    stk[regs.sp] = regs.ir->m;
    //d("stk[%d]=%d",regs.sp,regs.ir->m);
    status();
}

void OP_OPR(void) {
    switch((vm_opr)regs.ir->m) {
    case RET:
        frames[llev] = 0;
        llev--;

        regs.sp = regs.bp - 1;
        regs.pc = stk[regs.sp + 4];
        regs.bp = stk[regs.sp + 3];
        break;
    case NEG:
        stk[regs.sp] = -stk[regs.sp];
        break;
    case ADD:
        regs.sp--;
        stk[regs.sp] += stk[regs.sp + 1];
        break;
    case SUB:
        regs.sp--;
        stk[regs.sp] -= stk[regs.sp + 1];
        break;
    case MUL:
        regs.sp--;
        stk[regs.sp] *= stk[regs.sp + 1];
        break;
    case DIV:
        regs.sp--;
        stk[regs.sp] /= stk[regs.sp + 1];
        break;
    case ODD:
        if(stk[regs.sp] % 2) {
            stk[regs.sp] %= 2;
        }
        break;
    case MOD:
        regs.sp--;
        stk[regs.sp] %= stk[regs.sp + 1];
        break;
    case EQL:
        regs.sp--;
        stk[regs.sp] = (stk[regs.sp] == stk[regs.sp + 1]);
        break;
    case NEQ:
        regs.sp--;
        stk[regs.sp] = (stk[regs.sp] != stk[regs.sp + 1]);
        break;
    case LSS:
        regs.sp--;
        stk[regs.sp] = (stk[regs.sp] < stk[regs.sp + 1]);
        break;
    case LEQ:
        regs.sp--;
        stk[regs.sp] = (stk[regs.sp] <= stk[regs.sp + 1]);
        break;
    case GTR:
        regs.sp--;
        stk[regs.sp] = (stk[regs.sp] > stk[regs.sp + 1]);
        break;
    case GEQ:
        regs.sp--;
        stk[regs.sp] = (stk[regs.sp] >= stk[regs.sp + 1]);
        break;
    }
    status();
}

void OP_LOD(void) {
    regs.sp++;
    
    /* d("LOD: base(regs.ir->l, regs.bp) = base(%d,%d) = %d :: stk[%d] = stk[%d] == %d",
        regs.ir->l, regs.bp, base(regs.ir->l, regs.bp),
        regs.sp, base(regs.ir->l,regs.bp) + regs.ir->m, stk[base(regs.ir->l, regs.bp) + regs.ir->m]);
        // */
    stk[regs.sp] = stk[base(regs.ir->l, regs.bp) + regs.ir->m];
    status();
}
void OP_STO(void) {
    
    /* d("STO: base(regs.ir->l, regs.bp) = base(%d,%d) = %d :: stk[%d] = stk[%d] == %d",
        regs.ir->l, regs.bp, base(regs.ir->l, regs.bp), 
        base(regs.ir->l, regs.bp) + regs.ir->m, regs.sp, stk[regs.sp]);
        // */
    stk[base(regs.ir->l, regs.bp) + regs.ir->m] = stk[regs.sp];
    regs.sp--;
    status();
}
void OP_CAL(void) {
    stk[regs.sp + 1] = 0;
    stk[regs.sp + 2] = base(llev, regs.bp);
    stk[regs.sp + 3] = regs.bp;
    stk[regs.sp + 4] = regs.pc;
    regs.bp = regs.sp + 1;
    regs.pc = regs.ir->m;
    //d("frames[%d] = %d",llev,frames[llev]);
    llev++;
    frames[llev] = regs.bp;
    status();
}
void OP_INC(void) {
    regs.sp += regs.ir->m;
    status();
}
void OP_JMP(void) {
    regs.pc = regs.ir->m;
    status();
}
void OP_JPC(void) {
    if(stk[regs.sp] == 0) {
        regs.pc = regs.ir->m;
    }
    regs.sp--;
    status();
}
void OP_SIO(void) {
    switch(regs.ir->m) {
    case 1:
        print(stk[regs.sp]);
        regs.sp--;
        break;
    case 2:
        regs.sp++;
        stk[regs.sp] = read();
        break;
    }
    status();
}

